package com.task_html;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealTimeChatRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealTimeChatRestApplication.class, args);
	}

}
