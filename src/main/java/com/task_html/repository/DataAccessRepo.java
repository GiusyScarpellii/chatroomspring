package com.task_html.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface DataAccessRepo<T> {
	
	T insert(T t);
	List<T> findAll();
	boolean delete(int id);
}
