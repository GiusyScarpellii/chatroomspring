CREATE DATABASE realtime_chat_rest;
USE realtime_chat_rest;

CREATE TABLE Room(
	messaggio_id	INT 			NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    data_invio		DATETIME 		NOT NULL DEFAULT CURRENT_TIMESTAMP,
    username		VARCHAR(50) 	NOT NULL,
    testo			VARCHAR(1000) 	NOT NULL
);