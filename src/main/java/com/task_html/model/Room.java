package com.task_html.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Room {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="messaggio_id")
	private int id;
	
	@Column(name="data_invio", insertable = false, updatable = false)
	private String data;
	
	@Column
	private String username;
	
	@Column
	private String testo;

	
	public Room() {
		
	}

	
	public Room(int id, String data, String username, String testo) {
		
		this.id = id;
		this.data = data;
		this.username = username;
		this.testo = testo;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getData() {
		return data;
	}


	public void setData(String data) {
		this.data = data;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getTesto() {
		return testo;
	}


	public void setTesto(String testo) {
		this.testo = testo;
	}
	
	
	
	

}
