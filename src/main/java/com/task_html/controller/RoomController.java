package com.task_html.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task_html.model.Room;
import com.task_html.service.RoomService;

class StatusResponse{
	private String status;
	private Room mess;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Room getMess() {
		return mess;
	}
	public void setMess(Room mess) {
		this.mess = mess;
	}
}

@RestController
@RequestMapping("/messaggio")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RoomController {
	
	@Autowired
	private RoomService service;
	
	@PostMapping("/insert")
	public StatusResponse inserisciMessaggio(@RequestBody Room objMessaggio) {
		
		StatusResponse resp = new StatusResponse();
		Room temp = this.service.insert(objMessaggio);
		
		if(temp != null && temp.getId() > 0) {
			resp.setStatus("SUCCESS");
			resp.setMess(temp);
		}
		else {
			resp.setStatus("ERROR");
			resp.setMess(null);
		}
		
		return resp;
	}
	
	@GetMapping("/list")
	public List<Room> visualizzaChat() {
		return this.service.findAll();
	}
	
	@DeleteMapping("/{messaggio_id}")
	public boolean elimina_messaggio(@PathVariable int messaggio_id) {
		return service.delete(messaggio_id);
	}

}
