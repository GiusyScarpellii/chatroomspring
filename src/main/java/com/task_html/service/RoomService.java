package com.task_html.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task_html.model.Room;
import com.task_html.repository.DataAccessRepo;

@Service
public class RoomService implements DataAccessRepo<Room> {

	@Autowired
	private EntityManager entMan;						//Equivalente della Session Factoryh
	
	private Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	@Override
	public Room insert(Room t) {
		
		Room temp = new Room();
		temp.setUsername(t.getUsername());
		temp.setTesto(t.getTesto());
		
		Session session = getSession();
		
		try {
			session.save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		return temp;
		
	}

	@Override
	public List<Room> findAll() {
		Session session = getSession();
		return session.createCriteria(Room.class).list();
	}

	@Override
	public boolean delete(int varId) {
		
		Session session = getSession();
		
		try {
			
			Room temp = session.load(Room.class, varId);			//Va in exception se non trova l'oggetto sul DB!
		
			session.delete(temp);											//Ha marcato l'oggetto come DA ELIMINARE!
			session.flush();
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
	
	

}
